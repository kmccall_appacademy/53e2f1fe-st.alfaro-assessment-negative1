# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_numbers = []
  nums.each_with_index do |num, idx|
    break if num == nums.last
    if (nums[idx + 1] - num) > 1
      ((num+1)...nums[idx + 1]).each {|val| missing_numbers << val}
    end
  end
  missing_numbers
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  pow = 0
  res = 0
  binary.reverse.chars do |digit|
    res += digit.to_i * 2**pow
    pow += 1
  end
  res

end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    select_hash = Hash.new
    self.each do |k, v|
      select_hash[k] = v if proc.call(k, v)
    end
    select_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged_hash = Hash.new
    self.each do |k, v|
      value_to_merge = v
      if hash[k]
        if prc
          value_to_merge = prc.call(k,v,hash[k])
        end
      end
      merged_hash[k] = value_to_merge
    end
    hash.each do |k, v|
      if prc.nil? || self[k].nil?
        merged_hash[k] = v
      end
    end
    merged_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(num)
  lucas = [2, 1]
  if num >= 0
    return lucas[num] if num < 2
    (2..num).each do |number|
      lucas << (lucas[number - 1] + lucas[number - 2])
    end
    lucas.last
  else
    lucas.unshift(-1)
    n_shifts = 1
    (-2.downto(num)).each do |number|
      lucas.unshift(lucas[number + n_shifts + 2] - lucas[number + n_shifts + 1])
      n_shifts += 1
    end
    lucas.first
  end


end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  longest_palindrome = ""
  (0...string.length).each do |start_index|
    end_index = start_index + 2
    while end_index < string.length
      substring = string[start_index..end_index]
      if palindrome?(substring)
        if substring.length > longest_palindrome.length
          longest_palindrome = substring
        end
      end
      end_index += 1
    end
  end
  if longest_palindrome.empty?
    false
  else
    longest_palindrome.length
  end
end

def palindrome?(string)
  string.downcase == string.downcase.reverse
end
